(* Exercise 3.1 *)

type time_of_day_t = int * int * string;

infix 0 tBefore;

fun ((_, _, "AM"): time_of_day_t) tBefore ((_, _, "PM"): time_of_day_t) = true
  | (_, _, "PM") tBefore (_, _, "AM") = false
  | (h1, m1, f1) tBefore (h2, m2, f2) =
    f1=f2 andalso (h1<h2 orelse (h1=h2 andalso m1<m2));


type time_of_day_r = {hours: int, minutes: int, f: string};

infix 0 rBefore;

fun ({f = "AM", ...}: time_of_day_r) rBefore ({f = "PM", ...}: time_of_day_r) = true
  | {f = "PM", ...} rBefore {f = "AM", ...} = false
  | {hours = h1, minutes = m1, f = f1} rBefore {hours = h2, minutes = m2, f = f2} =
    f1=f2 andalso (h1<h2 orelse (h1=h2 andalso m1<m2));

(* Exercise 5.2 *)

fun rmodd [] = []
  | rmodd [x] = []
  | rmodd (x1::x2::xs) = x2::rmodd xs

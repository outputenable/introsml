(* 2016/12/06 *)

(* 2.1 *)

fun bin (n, 0) = 1
  | bin (n, k) = if n=k then 1 else bin (n-1, k-1) + bin (n-1, k);

(* 2.2 *)

fun pow (s, 0) = ""
  | pow (s, 1) = s
  | pow (s, n) = s ^ pow (s, n-1);

(* 2.3 *)

fun isIthChar (str, i, ch) = String.sub (str, i) = ch;

(* 2.4 *)

fun occFromIth (str, i, ch) =
  if i >= (size str) then 0
  else (if isIthChar (str, i, ch) then 1 else 0) + occFromIth (str, i+1, ch);

(* 2.5 *)

fun occInString (str, ch) = occFromIth (str, 0, ch);

(* 2.6 *)

fun notDivisible (d, n) = n mod d <> 0;

(* 2.7 *)

fun test (a, b, c) = a>b orelse notDivisible (a, c) andalso test (a+1, b, c);
fun prime n = n>1 andalso test (2, n-1, n);
fun nextPrime n = if prime (n+1) then n+1 else nextPrime (n+1);

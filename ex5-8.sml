(* Exercise 5.8 *)

fun number (x, ys) =
  let fun count ([], n) = n
        | count (y::ys, n) = count (ys, if x = y then n+1 else n)
  in
      count (ys, 0)
  end;

(* Exercise 3.2 *)

fun minCoins (pounds, shillings, pence) =
  let val totalPence = (pounds*20 + shillings) * 12 + pence
  in
      (Int.quot (totalPence, 20*12),
       Int.quot (Int.rem (totalPence, 20*12), 12),
       Int.rem (totalPence, 12))
  end;


infix 6 padd;

fun (pounds1, shillings1, pence1) padd (pounds2, shillings2, pence2) =
  minCoins (pounds1+pounds2, shillings1+shillings2, pence1+pence2);


infix 6 psub;

fun (pounds1, shillings1, pence1) psub (pounds2, shillings2, pence2) =
  minCoins (pounds1-pounds2, shillings1-shillings2, pence1-pence2);

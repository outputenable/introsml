(* Exercise 7.7 *)

datatype e = Intg of int | Intv of (int * int);
type intset = e list;

(* a *)

local
    fun asc (_, []) = true
      | asc (x, Intg y::ys) = x<y andalso asc (y, ys)
      | asc (x, Intv (lb, ub)::ys) = x<lb andalso lb<ub andalso asc (ub, ys)
in
    fun inv [] = true
      | inv (Intg x::xs) = asc (x, xs)
      | inv (Intv (lb, ub)::xs) = lb<ub andalso asc (ub, xs)
end

(* b *)

fun member (_, []) = false
  | member (x, Intg y::ys) = x=y orelse member (x, ys)
  | member (x, Intv (lb, ub)::ys) =
    (lb<=x andalso x<=ub) orelse member (x, ys)

fun add ([], y) = [Intg y]
  | add (Intg x::xs, y) =
    (case Int.compare (y, x) of
         LESS    => if y+1=x then Intv (y, x)::xs else Intg y::Intg x::xs
       | EQUAL   => Intg x::xs
       | GREATER => if x+1=y then Intv (x, y)::xs else Intg x::add (xs, y))
  | add (Intv (lb, ub)::xs, y) =
    if y<lb then
        if y+1=lb then Intv (y, ub)::xs else Intg y::Intv (lb, ub)::xs
    else if y<=ub then Intv (lb, ub)::xs
    else if ub+1=y then Intv (lb, y)::xs
    else Intv (lb, ub)::add (xs, y)

fun delete ([], _) = []
  | delete (Intg x::xs, y) =
    (case Int.compare (y, x) of
         LESS    => Intg x::xs
       | EQUAL   => xs
       | GREATER => Intg x::delete (xs, y))
  | delete (Intv (lb, ub)::xs, y) =
    if y<lb then Intv (lb, ub)::xs
    else if y<=ub then
        if y=lb then (if y+1=ub then Intg ub else Intv (y+1, ub))::xs
        else if y<ub then
            if lb+1=y then Intg lb::delete (Intv (y, ub)::xs, y)
            else if y+1=ub then Intv (lb, y-1)::Intg ub::xs
            else Intv (lb, y-1)::Intv (y+1, ub)::xs
        else (if lb=y-1 then Intg lb else Intv (lb, y-1))::xs
    else Intv (lb, ub)::delete (xs, y)

fun union (xs, []) = xs
  | union (xs, Intg y::ys) = union (add (xs, y), ys)
  | union (xs, Intv (lb, ub)::ys) =
    union (add (xs, lb), (if lb+1=ub then Intg ub else Intv (lb+1, ub))::ys)

fun intersection (_, []) = []
  | intersection ([], _) = []
  | intersection (xs, Intg y::ys) =
    let val intersect = intersection (xs, ys)
    in
        if member (y, xs) then add (intersect, y) else intersect
    end
  | intersection (xs, Intv (lb, ub)::ys) =
    (* Note that this recursively "blows up" the interval to distinct Intg
     * values! *)
    intersection (xs,
                  Intg lb::(if lb+1=ub then Intg ub else Intv (lb+1, ub))::ys)

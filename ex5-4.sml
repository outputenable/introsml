(* Exercise 5.4 *)

PolyML.use "ex2.sml";

fun pr n =
  let fun pp (0, primes) = primes
        | pp (m, []) = pp (m-1, [2])
        | pp (m, (x::xs)) = pp (m-1, nextPrime x::x::xs)
  in
      rev (pp (n, []))
  end;

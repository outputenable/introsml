(* Exercise 3.4 *)

type straightLine = real * real;

fun mirrorx ((a, b): straightLine) = (~a, ~b);

fun mirrory ((a, b): straightLine) = (~a, b);

fun toString ((a, b): straightLine) =
  let val slope = if Real.!= (a, 0.0) then Real.toString a ^ "x" else ""
      val intercept =
          if Real.!= (b, 0.0) then
              (if b>0.0 andalso Real.!= (a, 0.0) then "+" else "") ^ Real.toString b
          else if Real.== (a, 0.0) then "0.0"
          else ""
  in
      slope ^ intercept
  end;

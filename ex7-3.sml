(* Exercise 7.3 *)

fun partition (x, xs) =
  let fun part ([], p) = p
        | part (y::ys, (x1, x2, x3)) =
          case Int.compare (y, x) of
              LESS    => part (ys, (y::x1, x2, x3))
            | EQUAL   => part (ys, (x1, y::x2, x3))
            | GREATER => part (ys, (x1, x2, y::x3))
  in
      part (xs, ([], [], []))
  end

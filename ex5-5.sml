(* Exercise 5.5 *)

PolyML.use "ex2.sml";

fun pr' (m, n) =
  let fun pp (x, primes) =
        if x<n then pp (nextPrime x, x::primes) else primes
  in
      rev (pp (nextPrime m, []))
  end;

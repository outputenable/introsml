(* Exercise 7.4 *)

(* a *)

datatype mark = NOT_PASSED | PASSED | GRADE of int

(* b *)

fun isLegal PASSED = true
  | isLegal NOT_PASSED = true
  | isLegal (GRADE x) =
    x=0 orelse x=3 orelse (x>=5 andalso x<=11) orelse x=13

(* c *)

exception Illegal;

fun meanValue marks =
    let fun mean ([], sum, n) = real sum / real n
          | mean (m::ms, sum, n) =
            if not (isLegal m) then raise Illegal
            else case m of
                GRADE x => mean (ms, sum+x, n+1)
              | _       => mean (ms, sum, n)
    in
        mean (marks, 0, 0)
    end

(* d *)

fun numPassed marks =
  let fun passed m =
        if not (isLegal m) then raise Illegal
        else case m of
            PASSED  => true
          | GRADE x => x>=6
          | _       => false
      fun countPassed ([], n) = n
        | countPassed (m::ms, n) =
          countPassed (ms, if passed m then n+1 else n)
  in
      countPassed (marks, 0)
  end

(* Exercise 5.9 *)

fun split [] = ([], [])
  | split [x] = ([x], [])
  | split (x1::x2::xs) =
    let val (left, right) = split xs
    in
        (x1::left, x2::right)
    end;

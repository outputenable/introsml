(* Exercise 7.1 *)

datatype solution = NO_ROOTS | ONE_ROOT of real | TWO_ROOTS of real * real;

fun solve (a, b, c) =
  let val d = b*b - 4.0*a*c
  in
      if d < 0.0 orelse Real.== (a, 0.0) then NO_ROOTS
      else if Real.== (d, 0.0) then ONE_ROOT (~b / (2.0*a))
      else
          let val sqrtD = Math.sqrt d
          in
              TWO_ROOTS ((~b + sqrtD) / (2.0*a), (~b - sqrtD) / (2.0*a))
          end
  end

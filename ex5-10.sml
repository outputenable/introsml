(* Exercise 5.10 *)

fun prefix ([], _) = true
  | prefix (_, []) = false
  | prefix (x::xs, y::ys) = x = y andalso prefix (xs, ys)

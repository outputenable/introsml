(* Exercise 6.1 *)

(* a *)

fun multByConst (_, []) = []
  | multByConst (0, _) = [0]
  | multByConst (k, a::q) = k*a::multByConst (k, q)

(* b *)

fun multByX [] = []
  | multByX q = 0::q

(* c *)

fun addP (p, []) = p
  | addP ([], q) = q
  | addP (a::p, b::q) = a+b::addP (p, q)

fun multP (p, []) = []
  | multP ([], q) = []
  | multP (a::p, q) = addP (multByConst (a, q), multByX (multP (p, q)))

(* d *)

fun toString [] = ""
  | toString q =
    let fun termToString (a, 0) = Int.toString a
          | termToString (a, 1) = Int.toString a ^ "x"
          | termToString (a, n) = Int.toString a ^ "x**" ^ Int.toString n
        fun restToString ([], _) = ""
          | restToString (0::p, n) = restToString (p, n-1)
          | restToString (a::p, n) =
            (if a>0 then " + " else " - ") ^ termToString (abs a, n) ^ restToString (p, n-1)
        val p = rev q
        val n = length p - 1
    in
        termToString (hd p, n) ^ restToString (tl p, n-1)
    end;

(* Exercise 7.2 *)

fun smallest [] = NONE
  | smallest (x::xs) =
    case smallest xs of
        NONE   => SOME x
      | SOME y => SOME (if x<y then x else y)

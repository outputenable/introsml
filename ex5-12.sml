(* Exercise 5.12 *)

fun isPalindrome s = s = implode (rev (explode s))

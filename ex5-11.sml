(* Exercise 5.11 *)

fun lowerCaseOnly s =
  let fun allLowerCase [] = true
        | allLowerCase (c::cs) = Char.isLower c andalso allLowerCase cs
  in
      allLowerCase (explode s)
  end;

(* Exercise 5.1 *)

fun altsum [] = 0
  | altsum (x::xs) = x - (if null xs then 0 else hd xs - altsum (tl xs))

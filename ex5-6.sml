(* Exercise 5.6 *)

fun length [] = 0
  | length (x::xs) = 1 + length xs

fun nth ([], n) = raise Subscript
  | nth (x::xs, 0) = x
  | nth (x::xs, n) = nth (xs, n-1)

fun take (_, 0) = []
  | take ([], n) = raise Subscript
  | take (x::xs, n) = x::take (xs, n-1)

fun drop (xs, 0) = xs
  | drop ([], n) = raise Subscript
  | drop (x::xs, n) = drop (xs, n-1)

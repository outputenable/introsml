(* Exercise 5.13 *)

(* a *)

fun count (xs, y) =
  let fun doCount ([], n) = n
        | doCount ((x::xs), n) =
          if x < y then doCount (xs, n)
          else if x = y then doCount (xs, n+1)
          else n
  in
      doCount (xs, 0)
  end;

(* b *)

fun insert ([], y) = [y]
  | insert ((x::xs), y) = if x < y then x::insert (xs, y) else y::x::xs

(* c *)

fun intersect (_, []) = []
  | intersect ([], _) = []
  | intersect (x::xs, y::ys) =
    if x < y then intersect (xs, y::ys)
    else if x = y then x::intersect (xs, ys)
    else intersect (x::xs, ys)

(* d *)

fun plus (xs, []) = xs
  | plus ([], ys) = ys
  | plus (x::xs, y::ys) =
    if x <= y then x::plus (xs, y::ys) else y::plus (x::xs, ys)

(* e *)

fun minus (xs, []) = xs
  | minus ([], _) = []
  | minus (x::xs, y::ys) =
    if x < y then x::minus (xs, y::ys)
    else if x = y then minus (xs, ys)
    else minus (x::xs, ys)

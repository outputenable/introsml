(* Exercise 5.3 *)

fun combine [] = []
  | combine [x] = []
  | combine (x1::x2::xs) = (x1, x2)::combine xs

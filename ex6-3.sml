(* Exercise 6.3 *)

(* a *)

type country = string;
type neighbourRelation = (country * country) list;
type colour = country list;
type colouring = colour list;

(* b *)

(* fn : country * country * neighbourRelation -> bool *)
fun neighbours (_, _, []: neighbourRelation) = false
  | neighbours (c1, c2, (nb1, nb2)::nbs) =
    (c1=nb1 andalso c2=nb2) orelse (c1=nb2 andalso c2=nb1)
    orelse neighbours (c1, c2, nbs)

(* c *)

fun extendable ([], _, _) = true
  | extendable (_, _, []) = true
  | extendable (c1::cs, c2, nbr) =
    not (neighbours (c1, c2, nbr)) andalso extendable (cs, c2, nbr)

(* d *)

local
    (* from section 5.7 Membership; equality types *)
    infix member
    fun x member [] = false
      | x member (y::ys) = x=y orelse x member ys
in
    fun extend ([], ac, _) = [[ac]]
      | extend (col::cols, ac, nbr) =
        if ac member col then col::cols
        else if extendable (col, ac, nbr) then (ac::col)::cols
        else col::extend (cols, ac, nbr)
end;

(* e *)

fun colourMap nbr =
  let fun doColourMap ([], col) = col
        | doColourMap ((nb1, nb2)::nbs, col) =
          doColourMap (nbs, extend (extend (col, nb1, nbr), nb2, nbr))
  in
      doColourMap (nbr, [])
  end;

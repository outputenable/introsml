(* Exercise 5.14 *)

fun revrev [] = []
  | revrev (x::xs) = revrev xs @ [rev x]

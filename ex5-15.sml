(* Exercise 5.15 *)

fun sum (_, []) = 0
  | sum (p, x::xs) = (if p x then x else 0) + sum (p, xs)

(* Exercise 5.7 *)

fun rmeven [] = []
  | rmeven (x::xs) = if x mod 2 <> 0 then x::rmeven xs else rmeven xs

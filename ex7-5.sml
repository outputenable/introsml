(* Exercise 7.5 *)

datatype category =
         Day_nursery
       | Nursery_school
       | Recreation_centre

(* a *)

type name = string
type childDesc = name * category

(* b *)

fun number (_, []: childDesc list) = 0
  | number (c, (_, cat)::ds) =
    (if c=cat then 1 else 0) + number (c, ds)

(* c *)

fun totalMonthlyPayment siblings =
  let val charges = [(number (Day_nursery, siblings), 225.0),
                     (number (Nursery_school, siblings), 116.0),
                     (number (Recreation_centre, siblings), 100.0)]
      fun total ([], _) = 0.0
        | total ((n, p)::cs, discount) =
          if n>0 then real n * (p*discount) + total (cs, 0.5)
          else total (cs, discount)
  in
      total (charges, 1.0)
  end
